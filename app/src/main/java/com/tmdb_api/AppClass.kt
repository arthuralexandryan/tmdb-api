package com.tmdb_api

import android.app.Application
import com.android.post.di.module.NetworkModule
import com.tmdb_api.di.MovieModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppClass: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@AppClass)
            modules(listOf(MovieModule, NetworkModule))
        }
    }
}