package com.tmdb_api.data.model

data class MovieRequestModel(
    val id: Int,
    val image: String,
    val title: String,
    val subtitle: String,
    val date: String,
    val description: String
)