package com.tmdb_api.data.repository

import com.tmdb_api.data.model.MovieRequestModel
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    suspend fun getMovies(): Flow<List<MovieRequestModel>>

    suspend fun getMovie(id: Int): Flow<MovieRequestModel>
}