package com.tmdb_api.data.repository

import com.tmdb_api.data.api.MovieService
import com.tmdb_api.data.model.MovieRequestModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class MovieRepositoryImpl constructor(private val movieService: MovieService): MovieRepository{
    override suspend fun getMovies(): Flow<List<MovieRequestModel>> = flowOf(movieService.getMovies())

    override suspend fun getMovie(id: Int): Flow<MovieRequestModel> = flowOf(movieService.getMovie(id))
}