package com.tmdb_api.data.api

import com.tmdb_api.data.model.MovieRequestModel
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {
    @GET("movies")
    suspend fun getMovies(): List<MovieRequestModel>

    @GET("movies/{id}")
    suspend fun getMovie(@Path("id") movie_id: Int): MovieRequestModel
}