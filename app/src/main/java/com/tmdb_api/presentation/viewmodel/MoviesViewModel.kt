package com.tmdb_api.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tmdb_api.data.model.MovieRequestModel
import com.tmdb_api.data.repository.MovieRepositoryImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MoviesViewModel constructor(private val movieRepositoryImpl: MovieRepositoryImpl): ViewModel() {
    fun getMovies(): StateFlow<List<MovieRequestModel>?>{
        val data = MutableStateFlow<List<MovieRequestModel>?>(null)
        viewModelScope.launch {
            movieRepositoryImpl.getMovies()
                .collect { movies ->
                    data.value = movies
                }
        }
        return data
    }
}