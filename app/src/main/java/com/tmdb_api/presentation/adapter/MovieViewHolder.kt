package com.tmdb_api.presentation.adapter

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.tmdb_api.R
import com.tmdb_api.data.model.MovieRequestModel
import com.tmdb_api.models.MovieItemDto

class MovieViewHolder(private val view: View): RecyclerView.ViewHolder(view) {

    lateinit var onItemClick: () -> Unit

    fun onBind(item: MovieItemDto) {
        view.findViewById<ConstraintLayout>(R.id.item_container).setOnClickListener {
            onItemClick.invoke()
        }
        view.findViewById<AppCompatTextView>(R.id.tv_item_title).text = item.title
        view.findViewById<AppCompatTextView>(R.id.tv_item_subtitle).text = item.subtitle
    }
}