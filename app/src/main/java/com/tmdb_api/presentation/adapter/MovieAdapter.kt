package com.tmdb_api.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tmdb_api.R
import com.tmdb_api.models.MovieItemDto

class MovieAdapter : ListAdapter<MovieItemDto, RecyclerView.ViewHolder>(MOVIE_COMPARATOR) {

    lateinit var onItemClick: () -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder){
            holder.onBind(getItem(position))
        }
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        if (holder is MovieViewHolder){
            holder.onItemClick = {
                onItemClick.invoke()
            }
        }
    }

    companion object {
        val MOVIE_COMPARATOR = object : DiffUtil.ItemCallback<MovieItemDto>() {
            override fun areItemsTheSame(oldItem: MovieItemDto, newItem: MovieItemDto): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: MovieItemDto, newItem: MovieItemDto): Boolean =
                oldItem == newItem
        }
    }
}