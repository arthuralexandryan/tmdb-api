package com.tmdb_api.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.tmdb_api.R
import com.tmdb_api.models.MovieItemDto
import com.tmdb_api.presentation.adapter.MovieAdapter
import com.tmdb_api.presentation.viewmodel.MoviesViewModel
import com.tmdb_api.utils.AppExt.initRecyclerView
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoviesActivity : AppCompatActivity() {

    private val moviesViewModel: MoviesViewModel by viewModel()
    private val adapter: MovieAdapter = MovieAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        initRecyclerView()
        getMovies()
    }

    private fun getMovies() {
        lifecycleScope.launchWhenCreated {
            moviesViewModel.getMovies()
                .collect { response ->
                    response?.run {
                        val list = this.map { item ->
                            MovieItemDto(
                                title =  item.title,
                                subtitle = item.subtitle,
                                image = item.image
                            )
                        }
                        adapter.submitList(list)
                    }
                }
        }
    }

    private fun initRecyclerView() {
        findViewById<RecyclerView>(R.id.rv_movies).initRecyclerView(this, adapter)
    }
}