package com.android.post.di.module

import com.tmdb_api.data.api.MovieService
import com.tmdb_api.data.repository.MovieRepository
import com.tmdb_api.data.repository.MovieRepositoryImpl
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit


private const val TIME_OUT = 30L

val NetworkModule = module {

    single { createService(get()) }

    single { createRetrofit(get(), "url") }

    single { createOkHttpClient() }

}

fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(TIME_OUT, TimeUnit.SECONDS).build()
}

fun createRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient).build()
}

fun createService(retrofit: Retrofit): MovieService {
    return retrofit.create(MovieService::class.java)
}

fun createMoviesRepository(apiService: MovieService): MovieRepository {
    return MovieRepositoryImpl(apiService)
}
