package com.tmdb_api.di

import com.android.post.di.module.createMoviesRepository
import com.tmdb_api.presentation.viewmodel.MovieDetailViewModel
import com.tmdb_api.presentation.viewmodel.MoviesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val MovieModule = module {
    viewModel { MoviesViewModel(get())}
    viewModel { MovieDetailViewModel(get())}

    single { createMoviesRepository(get()) }
}
