package com.tmdb_api.utils

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tmdb_api.data.model.MovieRequestModel
import com.tmdb_api.models.MovieItemDto

object MovieMapper {
    fun MovieRequestModel.toMovieItemDto() = MovieItemDto(
        title = this.title,
        subtitle = this.subtitle,
        image = this.image
    )
}