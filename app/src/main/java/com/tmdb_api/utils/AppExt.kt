package com.tmdb_api.utils

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

object AppExt {

    fun <R> RecyclerView.initRecyclerView(
        context: Context?,
        adapter: R,
        isVertical: Boolean = true,
        isSizeFixed: Boolean = true,
        isNestedScrollingEnabled: Boolean = false
    ) {
        context?.let {
            this.layoutManager = LinearLayoutManager(
                context,
                if (isVertical) LinearLayoutManager.VERTICAL else LinearLayoutManager.HORIZONTAL,
                false
            )
            this.adapter = adapter as RecyclerView.Adapter<*>
            this.setHasFixedSize(isSizeFixed)
            this.isNestedScrollingEnabled = isNestedScrollingEnabled
        }
    }
}