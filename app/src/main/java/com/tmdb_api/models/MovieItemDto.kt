package com.tmdb_api.models

data class MovieItemDto(val title: String?, val subtitle: String?, val image: String?)
